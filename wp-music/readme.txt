=== Wp-music ===
Contributors:pankaj


Most customizable. Developer-friendly. Active support with  simple Shortcode Generator.


== Description ==

Music



= FEATURES OF THIS PLUGIN =

custom post type Music
* Custom hierarchical taxonomy Genre
* Custom non-hierarchical taxonomy Music Tag
* Custom meta box to save music meta information like Composer Name, Publisher, Year of recording, Additional Contributors, URL, Price, etc.
* Create a custom meta table and save all music meta information in that table.
* Create a custom admin settings page for Music. Settings option should contain
options for changing currency, number of musics displayed per page, etc.
Settings menu should be displayed under the Musics menu.