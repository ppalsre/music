<?php
/**
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://shapedplugin.com
 * @since             1.0
 * @package           Testimonial
 *
 * Plugin Name:     WP Music
 * Plugin URI:      
 * Description:     Music based plugin to add meta tags.
 * Version:         2.2.18
 * Author:          pankaj pal
 * Text Domain:     wp-music
 
 */

add_action( 'init', 'create_music_custom_post');

function create_music_custom_post() {
    register_post_type( 'music',
        array(
            'labels' => array(
                'name' => 'Music',
                'singular_name' => 'Movie List',
                'add_new' => 'Add New',
                'add_new_item' => 'Add  Music Data',
                'edit' => 'Edit',
                'edit_item' => 'Edit Music Data',
                'new_item' => 'Add Music',
                'view' => 'View',
                'view_item' => 'View Music',
                'search_items' => 'Search Music',
                'not_found' => 'No Music data found',
                'not_found_in_trash' => 'No Movie Reviews found in Trash',
                'parent' => 'Parent Movie Review'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

//hook into the init action and call create_genre_hierarchical_taxonomy when it fires
 
add_action( 'init', 'create_genre_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it genre for custom post type music
 //Genre
function create_genre_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Genre', 'taxonomy general name' ),
    'singular_name' => _x( 'Genre', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Genre' ),
    'all_items' => __( 'All Genre' ),
    'parent_item' => __( 'Parent Genre' ),
    'parent_item_colon' => __( 'Parent Genre:' ),
    'edit_item' => __( 'Edit Genre' ), 
    'update_item' => __( 'Update Genre' ),
    'add_new_item' => __( 'Add New Genre' ),
    'new_item_name' => __( 'New Genre Name' ),
    'menu_name' => __( 'Genre' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('genre',array('music'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'genre' ),
  ));
 
}

//hook into the init action and call create_musictag_nonhierarchical_taxonomy when it fires
 
add_action( 'init', 'create_musictag_nonhierarchical_taxonomy', 0 );
 
function create_musictag_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Music Tag', 'taxonomy general name' ),
    'singular_name' => _x( 'Music Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Music Tag' ),
    'popular_items' => __( 'Popular Music Tag' ),
    'all_items' => __( 'All Music Tag' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Music Tag' ), 
    'update_item' => __( 'Update Music Tag' ),
    'add_new_item' => __( 'Add New Topic' ),
    'new_item_name' => __( 'New Music Tag Name' ),
    'separate_items_with_commas' => __( 'Separate music tag with commas' ),
    'add_or_remove_items' => __( 'Add or remove music tags' ),
    'choose_from_most_used' => __( 'Choose from the most used music tags' ),
    'menu_name' => __( 'Music Tag' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('musictag','music',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'musictag' ),
  ));
}

add_action( 'add_meta_boxes', 'music_meta_box_add' );

function music_meta_box_add()
{
    add_meta_box( 'music-meta-box-id', 'Music Data Save', 'music_meta_box_cb', 'music', 'normal', 'high' );
}

function music_meta_box_cb($id = NULL)
{
    $post_id = !empty($_GET['post'])?$_GET['post']:'';

    if (!empty($_GET['post'])) {

        $action = 'edit';
        $post_id = $_GET['post'];

    }
    else {

        $action = 'add';
        $post_id = '';

    }
    global $wpdb;
    $composer = '';
    $publisher = '';
    $recording_yearmusic = '';
    $contributor = '';
    $url = '';
    $price = '';
    // Shortcodes RETURN content, so store in a variable to return
    $content = '<table>';
    $content .= '';
    $sql_query = "SELECT * FROM cu_music_meta where music_id = ".$post_id;

    $results = $wpdb->get_results($sql_query);

    if(!empty($results)){

        foreach($results as $value){

            $composer .= ($value->meta_key == 'composer') ?$value->meta_value:'';
            $publisher .= ($value->meta_key=='publisher')?$value->meta_value:'';
            $recording_yearmusic .= ($value->meta_key=='recording_yearmusic')?$value->meta_value:'';
            $contributor .= ($value->meta_key=='contributor')?$value->meta_value:'';
            $url .= ($value->meta_key=='url')?$value->meta_value:'';
            $price .= ($value->meta_key=='price')?$value->meta_value:'';


        }
    }

    $str = '';
    $str .='<input type="hidden" value="'.$action.'"   name="action" /><label for="music_meta_box_composer" style="width:100px">Composer Name</label>
    <input type="text" value="'.$composer.'"  style ="margin:20px 40px 20px;10px;" name="music[composer]" id="music_meta_box_composer" /><br>';

    $str .='<label for="music_meta_box_publisher">Publisher</label>
    <input type="text" value="'.$publisher.'"   style ="margin:20px 40px 20px;10px;" name="music[publisher]" id="music_meta_box_publisher" /><br>';
    
    $str .='<label for="music_meta_box_recording_year">Year of recording</label>
    <input type="text" value="'.$recording_yearmusic.'"    name="music[recording_yearmusic]" style ="margin:20px 40px 20px;10px;" id="music_meta_box_recording_year" /><br>';
    
    $str .='<label for="music_meta_box_contributor">Additional Contributors</label>
    <input type="text" value="'.$contributor.'" name="music[contributor]" style ="margin:20px 40px 20px;10px;" id="music_meta_box_contributor" /><br>';

    $str .='<label for="music_meta_box_url">URL</label>
    <input type="text" value="'.$url.'"  name="music[url]" style ="margin:20px 40px 20px;10px;" id="music_meta_box_url" /><br>';

    $str .='<label for="music_meta_box_price">PRICE</label>
    <input type="text" value="'.$price.'"  name="music[price]" id="music_meta_box_price"  style ="margin:20px 40px 20px;10px;"/>';
    echo $str;   
}

$your_db_name = $wpdb->prefix . 'wp';
 
// function to create the DB / Options / Defaults					
function your_plugin_options_install() {
   	global $wpdb;
  	global $your_db_name;
 
	// create the ECPT metabox database table
	if($wpdb->get_var("show tables like '$your_db_name'") != $your_db_name) 
	{
		$sql .= "CREATE TABLE {$wpdb->prefix}music_meta (
		meta_id bigint(20) NOT NULL AUTO_INCREMENT,
		music_id bigint(20) NOT NULL DEFAULT '0',
		meta_key varchar(255) DEFAULT NULL,
		meta_value longtext,
		PRIMARY KEY meta_id (meta_id),
		KEY music_id (music_id),
		KEY meta_key (meta_key)
		) CHARACTER SET utf8 COLLATE utf8_general_ci;";
 
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
 	// Registering meta table
    
}

function save_my_custom_data( $post_id ){
	
    global $wpdb;

    if(!empty( $_POST['music']) && ($_POST['post_type'] == 'music') ){

        foreach($_POST['music'] as $key=>$value){
     				
     				$data = [];

					
                    if($_POST['action'] == 'edit') {
                        $data['meta_value'] = $value;
                        $data = array_filter( $data );
                        $where = [ 'music_id' => $_POST['post_ID'] , 'meta_key' =>$key ];
                        $wpdb->update( 'cu_music_meta', $data ,$where);
                       

                    }
                    else {
                        $data['music_id'] = $_POST['post_ID'];
                        $data['meta_key'] = $key;
                        $data['meta_value'] = $value;
                        $data = array_filter( $data );
                        $wpdb->insert( 'cu_music_meta', $data );
                    }
                }
     		
    	}
	}

add_action( 'admin_init', 'save_my_custom_data' );
// run tqhe install scripts upon plugin activation
register_activation_hook(__FILE__,'your_plugin_options_install');
add_action( 'admin_menu', 'music_options_page' );


function music_options_page() {
	register_setting(
		'music_settings', // settings group name
		'homepage_text', // option name
		'sanitize_text_field' // sanitization function
	);
		add_submenu_page(
		    'edit.php?post_type=music',
		    __( 'Music Settings', 'menu-test' ),
		    __( 'Music Settings', 'menu-test' ),
			'manage_options',
		    'music-slug',
		    'music_page_content'
		);

	// add_options_page(
	// 	'Music Setting ', // page <title>Title</title>
	// 	'Music Setting', // menu .                           W2WSXQCfcrawvtfcdxazwVFVS link text
	// 	'manage_options', // capability to access the page
	// 	'music-slug', // page URL slug
	// 	'music_page_content', // callback function with content
	// 	0 // priority
	// );

}

function music_page_content(){

	echo '<div class="wrap">
	<h1>Music Page Settings</h1>
	<form method="post" action="options.php"><table class="form-table">
        <tr valign="top">
        <th scope="row">Changing Currency</th>
        <td><input type="text" name="changing_currency" value="'.esc_attr( get_option('changing_currency') ).'" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Number of musics displayed per page</th>
        <td><input type="text" name="no_music_per_page" value="'. esc_attr( get_option('no_music_per_page') ).'" /></td>
        </tr>
        
    </table>';
			
		settings_fields( 'music_settings' ); // settings group name
		do_settings_sections( 'music-slug' ); // just a page slug

		submit_button();

	echo '</form></div>';

}

// add the shortcode [pontoon-table], tell WP which function to call
add_shortcode( 'music', 'music_shortcode_table' );

// this function generates the shortcode output
function music_shortcode_table() {

    global $wpdb;
    // Shortcodes RETURN content, so store in a variable to return
    $content = '<table>';
    $content .= '';
    $results = $wpdb->get_results( ' SELECT * FROM cu_music_meta' );

    foreach ( $results AS $row ) {
        $content .= '<div class="main_div">';
        // Modify these to match the database structure
         $content .= '<div class="composer">' . ($row->meta_key=='composer')?$row->meta_value.' '.'':'' . '</div>';
         $content .= '<div class="publisher">' . ($row->meta_key=='publisher')?$row->meta_value:'' . '</div>';
         $content .= '<div class="recording_yearmusic">' . ($row->meta_key=='recording_yearmusic')?$row->meta_value:'' . '</td>';
         $content .= '<div class="contributor">' . ($row->meta_key=='contributor')?$row->meta_value:'' . '</div>';
        $content .= '<div class="url">' . ($row->meta_key=='url')?$row->meta_value:'' . '</div>';
         $content .= '<div class="price">' . ($row->meta_key=='price')?$row->meta_value:'' . '</div>';
        $content .= '</div>';
    }
    $content .= '</table>';
    return $content;
}





